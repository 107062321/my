# Writing Template

- 然而 However = in contrast = nevertheless = while = whereas = notwithstanding = yet
- 因此 Therefore = thus = as a result = hence = accordingly = consequently
- 雖然 Although = despite = while = in spite of = though = whereas
- 例如 For instance = for example = take…as an example = such as
- 首先 First of all = To begin with
- 再加上 In addition = besides = moreover = furthermore
- 相反地 In contrast = on the other hand = on the contrary

# Integrated Writing

## Notes

| Reading | Listening |
| --- | --- |
| Issue | Argument |
| Reason1 | Reason1 |
| Reason2 | Reason2 |
| Reason3 | Reason3 |

Reading 會在旁邊，可以不用抄下來

## Template

P1<br>
The reading passage explores the issue of [Reading Issue], and several reasons are
provided in support of the argument.
Although the author's argument seems substantiated, the lecturer casts
doubts on it for solid reasons.

P2<br>
First, even though the reading passage suggests that [Reading Reason 1], the lecturer claims that
[Listening Reason 1].
He states that [Supporting Details].
Therefore, the speaker’s argument disapproves of its counterpart in the reading passage.

P3<br>
Second, the statement held by the writer indicates that [Reading Reason 2].
Conversely, the speaker refutes the idea by a valid reason that [Listening
Reason 2].
He concludes that the second argument in the reading overlooks [Supporting Details].
In essence, there is no evidence that [Supporting Details].

P4<br>
Lastly, the lecturer precisely identifies the weakness in the reading that [Reading Reason 3].
He convincingly points out that [Listening Reason 3].
In order to establish a concrete statement, he provides an example that [Supporting
Details].
As a result, [Listening Reason 3] is a poorly-formulated thesis.

P5<br>
Based on the evidence provided above, it can be clearly seen that stances on both sides are
paradoxical.
While the claims in the reading passage seem plausible, the speaker disproves them by
compelling reasons.

# Independent Writing

P1<br>
When it comes to [Statement], answers can vary drastically depending on one's
background.
Some argue that [ ], while others believe that [ ].
While both sides seem convincing and plausible, my personal experiences have led me
to conclude that [ ].

P2<br>
First, [Reason 1 Topic Sentence]. [Reason 1 Supporting Details]. For example, [Ex1].

P3<br>
Second, [Reason 2 Topic Sentence]. [Reason 2 Supporting Details]. For instance, [Ex2].

P4<br>
Lastly, [Reason 3 Topic Sentence]. [Reason 3 Supporting Details]. Take [Ex3] as an
example.

P5<br>
Given the reasons mentioned above, I strongly believe that [ ].
That is not to say that other opinions are entirely without merit.
However, I believe I have substantiated reasons to support my point of view.
